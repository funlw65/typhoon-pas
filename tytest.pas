program tytest;

uses cmem, sysutils,
{$define TYPHOON_STATIC} // if you use the static library
ty,
dbtest;

{$ifdef TYPHOON_STATIC}
  {$linklib c}
{$endif}

var
  team : people;

procedure print_record;
begin
  write(team.name +' | ');
  write(team.birth + ' | ');
  writeln(team.location);
end;

begin
  {$I-}
  if(not DirectoryExists('data')) then
    MkDir('data');
  {$I+}
  //  
  writeln('Typhoon relational database test v1.0.');
  d_dbfpath('data');
  if(d_open('dbtest','x') = S_OKAY) then begin
    // adding records
    writeln('Adding 10 records.');
    writeln('');
    team.name := 'Tudor Vadim'; team.birth := '1954-03-22'; team.location := 'Moscow, Russia';
    if (d_fillnew(PEOPLE_TBL, @team) = S_OKAY) then writeln('  Added record nr.  1 '+'('+team.name+')')
    else writeln('  Error adding record nr. 1');

    team.name := 'Anamaria Prodan'; team.birth := '2004-04-04'; team.location := 'Bucharest. Romania';
    if (d_fillnew(PEOPLE_TBL, @team) = S_OKAY) then writeln('  Added record nr.  2 '+'('+team.name+')')
    else writeln('  Error adding record nr. 2');

    team.name := 'Pascal Kunor'; team.birth := '2000-12-12'; team.location := 'Budapest, Hungary';
    if (d_fillnew(PEOPLE_TBL, @team) = S_OKAY) then writeln('  Added record nr.  3 '+'('+team.name+')')
    else writeln('  Error adding record nr. 3');

    team.name := 'Shin Beth'; team.birth := '1969-06-01'; team.location := 'Jerusalem, Israel';
    if (d_fillnew(PEOPLE_TBL, @team) = S_OKAY) then writeln('  Added record nr.  4 '+'('+team.name+')')
    else writeln('  Error adding record nr. 4');

    team.name := 'Eren Hodomor'; team.birth := '1729-12-31'; team.location := 'Wesmere, Wesnoth';
    if (d_fillnew(PEOPLE_TBL, @team) = S_OKAY) then writeln('  Added record nr.  5 '+'('+team.name+')')
    else writeln('  Error adding record nr. 5');

    team.name := 'Andrei Popescu'; team.birth := '2002-05-24'; team.location := 'Sidney, Australia';
    if (d_fillnew(PEOPLE_TBL, @team) = S_OKAY) then writeln('  Added record nr.  6 '+'('+team.name+')')
    else writeln('  Error adding record nr. 6');

    team.name := 'Kalenz Frogmorton'; team.birth := '1954-11-26'; team.location := 'Wesmere, Wesnoth';
    if (d_fillnew(PEOPLE_TBL, @team) = S_OKAY) then writeln('  Added record nr.  7 '+'('+team.name+')')
    else writeln('  Error adding record nr. 7');

    team.name := 'Elen Tiperay'; team.birth := '2019-01-01'; team.location := 'London, Great Britain';
    if (d_fillnew(PEOPLE_TBL, @team) = S_OKAY) then writeln('  Added record nr.  8 '+'('+team.name+')')
    else writeln('  Error adding record nr. 8');

    team.name := 'Abdul Hasan'; team.birth := '1999-04-08'; team.location := 'Cairo, Egypt';
    if (d_fillnew(PEOPLE_TBL, @team) = S_OKAY) then writeln('  Added record nr.  9 '+'('+team.name+')')
    else writeln('  Error adding record nr. 9');

    team.name := 'Zan Hun'; team.birth := '2001-12-22'; team.location := 'Beijing, China';
    if (d_fillnew(PEOPLE_TBL, @team) = S_OKAY) then writeln('  Added record nr. 10 '+'('+team.name+')')
    else writeln('  Error adding record nr. 10');
    writeln('');
    // dsplay the table content after the key.
    writeln('Display the table content after the key order. '); writeln;
    if(d_keyfrst(NAME_FLD) = S_OKAY) then begin
      if(d_recread(@team) = S_OKAY) then begin
        print_record;
        while((d_keynext(NAME_FLD)) = S_OKAY) do begin
          d_recread(@team);
          print_record;
        end;
      end
      else writeln('Error d_recread!');
    end
    else writeln('Error d_keyfrst!');
    //
    writeln('');
    writeln('Deleting the records using the "record navigator"!');
    write('  ');
    while(d_recfrst(PEOPLE_TBL) = S_OKAY) do begin
      if(d_delete() = S_OKAY) then 
        write('* ')
      else writeln('Error d_delete!');
    end;
    writeln;
    writeln('Done! Test ended.');
    //
    d_close();  
  end
  else writeln('Error opening the database!');
end.
