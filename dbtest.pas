unit dbtest;
interface

{
  Automatically converted by H2Pas 1.0.0 from dbtest.h
  The following command line parameters were used:
    dbtest.h
}

{$IFDEF FPC}
{$PACKRECORDS C}
{$ENDIF}


  {---------- headerfile for dbtest.ddl ---------- }
  { alignment is 8  }
  {---------- structures ---------- }
  { size 180  }

  type
    people = record
        name : array[0..69] of char;
        birth : array[0..9] of char;
        location : array[0..99] of char;
      end;

  {---------- record names ---------- }

  const
    PEOPLE_TBL = 1000;    
  {---------- field names ---------- }
    NAME_FLD = 1001;    
    BIRTH_FLD = 1002;    
    LOCATION_FLD = 1003;    
  {---------- key names ---------- }
  {---------- sequence names ---------- }
  {---------- integer constants ---------- }

implementation


end.
