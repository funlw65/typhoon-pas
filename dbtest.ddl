database dbtest{

  data file "team.dat" contains people;
  key  file "team.ix1" contains people.name;
  
  record people{
    char name[70];
    char birth[10];
    char location[100];
    
    primary key name;
  }
}

  
  