# Description

This is a FreePascal unit (__ty.pas__) that provides bindings for Typhoon Relational Database C library which also comes with it's own set of database management tools.

You can download the typhoon project from [here](https://gitlab.com/funlw65/osdb-typhoon).
Compile and install (in /usr/local) not only the typhoon library, but also it's tools that are:

- dbdview
- ddlp
- tyexport
- tyimport

**Note:** the library is statically linked.

# How it works (a demo is provided in repository):

## Create and process a database definition file 

Create a .ddl type file (which is a regular text file) in your project's folder, named, by example, __dbtest.ddl__. It has the following content:

~~~
database dbtest{

  data file "team.dat" contains people;
  key  file "team.ix1" contains people.name;
  
  record people{
    char name[70];
    char birth[10];
    char location[100];
    
    primary key name;
  }
}
~~~

I won't explain it's content, you have to study the typhoon documentation.

Process it with the following command (supposing that you installed also the typhoon tools):

~~~
ddlp dbtest.ddl
~~~

As a result, you will get two files:

- dbtest.dbd - which is a binary representation of the database definitions;
- dbtest.h - which is a C header with the required definitions (see it below).

dbtest.h content:

~~~c
/*---------- headerfile for dbtest.ddl ----------*/
/* alignment is 8 */

/*---------- structures ----------*/
struct people {  /* size 180 */
    char    name[70];
    char    birth[10];
    char    location[100];
};

/*---------- record names ----------*/
#define PEOPLE 1000L

/*---------- field names ----------*/
#define NAME 1001L
#define BIRTH 1002L
#define LOCATION 1003L

/*---------- key names ----------*/

/*---------- sequence names ----------*/

/*---------- integer constants ----------*/
~~~


## Convert the C header to Pascal language

For this, issue the following command:

~~~
h2pas dbtest.h
~~~

You will get a pascal file with .pp extension which I change it to .pas as a personal preference (dbtest.pas).

__Now__, because Pascal language is not case sensitive, we will get naming conflicts so, we have to edit the file to solve the problems in the following manner:

For every record name we add "_TBL" suffix and for every field name we add "_FLD" suffix (or whatever you like), as bellow:

~~~pascal
unit dbtest;
interface

{
  Automatically converted by H2Pas 1.0.0 from dbtest.h
  The following command line parameters were used:
    dbtest.h
}

{$IFDEF FPC}
{$PACKRECORDS C}
{$ENDIF}


  {---------- headerfile for dbtest.ddl ---------- }
  { alignment is 8  }
  {---------- structures ---------- }
  { size 180  }

  type
    people = record
        name : array[0..69] of char;
        birth : array[0..9] of char;
        location : array[0..99] of char;
      end;

  {---------- record names ---------- }

  const
    PEOPLE_TBL = 1000;    
  {---------- field names ---------- }
    NAME_FLD = 1001;    
    BIRTH_FLD = 1002;    
    LOCATION_FLD = 1003;    
  {---------- key names ---------- }
  {---------- sequence names ---------- }
  {---------- integer constants ---------- }

implementation


end.
~~~ 

Save the modifications and we are ready for our next step:

## Creating an application

The following code fragment contains the minimum required for a simple console application:

~~~pascal
program tytest;

uses cmem, sysutils,
{$DEFINE TYPHOON_STATIC} // add this before the ty unit if you use the static library
ty,
dbtest;

{$ifdef TYPHOON_STATIC}
  {$linklib c}
{$endif}

~~~

I won't continue further, for a functional demo, see the tytest.pas file in repository which you compile with the following command:

~~~
fpc tytest.pas
~~~

Though, I will add that the TYPHOON_STATIC definition was moved outside the unit,
on the application side, to avoid having the user edit the unit every time he 
changes between static and dynamic/shared libs.
 
After a successful compilation, run it into the console and you should get this:

~~~
Typhoon relational database test v1.0.
Adding 10 records.

  Added record nr.  1 (Tudor Vadim)
  Added record nr.  2 (Anamaria Prodan)
  Added record nr.  3 (Pascal Kunor)
  Added record nr.  4 (Shin Beth)
  Added record nr.  5 (Eren Hodomor)
  Added record nr.  6 (Andrei Popescu)
  Added record nr.  7 (Kalenz Frogmorton)
  Added record nr.  8 (Elen Tiperay)
  Added record nr.  9 (Abdul Hasan)
  Added record nr. 10 (Zan Hun)

Display the table content after the key order. 

Abdul Hasan | 1999-04-08 | Cairo, Egypt
Anamaria Prodan | 2004-04-04 | Bucharest. Romania
Andrei Popescu | 2002-05-24 | Sidney, Australia
Elen Tiperay | 2019-01-01 | London, Great Britain
Eren Hodomor | 1729-12-31 | Wesmere, Wesnoth
Kalenz Frogmorton | 1954-11-26 | Wesmere, Wesnoth
Pascal Kunor | 2000-12-12 | Budapest, Hungary
Shin Beth | 1969-06-01 | Jerusalem, Israel
Tudor Vadim | 1954-03-22 | Moscow, Russia
Zan Hun | 2001-12-22 | Beijing, China

Deleting the records using the "record navigator"!
  * * * * * * * * * * 
Done! Test ended.
~~~

This is all, happy exploring!

BTW, for a more elaborate demo (adding, modifying, deleting, finding and navigating through the records), with a nice graphical UI, see [here](https://gitlab.com/funlw65/squabdb-pas).

__Final Note:__ everything was developed under a Debian 11 compatible linux system. If your Linux system is older, you have to compile and replace the libtyphoon.a in repository and recompile the pascal units and the test application. 

To clean it (after you replaced the libtyphoon.a with your own):

~~~
rm tytest
rm *.o
rm *.ppu
fpc tytest.pas
~~~
