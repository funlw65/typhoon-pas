/*---------- headerfile for dbtest.ddl ----------*/
/* alignment is 8 */

/*---------- structures ----------*/
struct people {  /* size 180 */
    char    name[70];
    char    birth[10];
    char    location[100];
};

/*---------- record names ----------*/
#define PEOPLE 1000L

/*---------- field names ----------*/
#define NAME 1001L
#define BIRTH 1002L
#define LOCATION 1003L

/*---------- key names ----------*/

/*---------- sequence names ----------*/

/*---------- integer constants ----------*/
